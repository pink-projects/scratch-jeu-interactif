# Intégration Scratch dans HTML

## Aperçu
Ce projet consiste en une page HTML qui intègre un jeu interactif Scratch spécifique. Ce jeu offre plusieurs scénarios qui évoluent selon les choix de l'utilisateur, rendant chaque partie unique. 

PS: Un des scénarios nécessite spécifiquement l'utilisation du clic de la souris pour progresser ou interagir avec les éléments du jeu.

## Fonctionnalités
- **Jeu Interactif à Scénarios Multiples** : Propose une expérience de jeu dynamique où les choix et les interactions, y compris les clics de souris, influencent le déroulement de l'histoire.
- **Intégration Fullscreen** : Maximise l'espace visuel disponible pour une immersion totale dans le jeu grâce à des styles CSS optimisés.

## Structure du Projet
- `Scratch.html` : Fichier HTML intégrant le jeu Scratch via un iframe.
- `Scratch.css` : Feuille de style CSS qui assure que l'iframe occupe toute la fenêtre du navigateur pour une expérience utilisateur améliorée.

## Installation et Utilisation
- **Visualisation** : Ouvrez `Scratch.html` dans un navigateur moderne pour démarrer le jeu.
- **Interaction** : Utilisez le clic de la souris pour naviguer et faire des choix dans les scénarios qui le requièrent, ce qui est essentiel pour avancer dans certaines parties du jeu.

## Prérequis
- Un navigateur web moderne supportant HTML5 et JavaScript, tel que Chrome, Firefox, Safari, ou Edge.

## Support
Si vous rencontrez des difficultés techniques ou avez des questions, assurez-vous que votre navigateur permet les contenus externes et les iframes. Pour des questions spécifiques à Scratch, consultez la documentation de Scratch ou contactez le support communautaire.
